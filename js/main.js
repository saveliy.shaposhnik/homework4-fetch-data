const URI = "https://swapi.dev/api/films/";

const planetsList = document.createElement("div");
document.body.append(planetsList);

function getPlanets() {
  return fetch(URI, {
    method: "GET",
  }).then((response) => response.json());
}

getPlanets().then(({ results }) => {
  console.log(results);
  results.forEach(({ title, opening_crawl, characters, episode_id }) => {
    const film = document.createElement("ul");
    const filmTitle = document.createElement("li");
    const episode = document.createElement("li");
    const shortInfo = document.createElement("li");
    const filmCharacters = document.createElement("ul");

    filmTitle.textContent = title;
    episode.textContent = `Episode ${episode_id}`;
    shortInfo.textContent = opening_crawl;
    planetsList.append(film);
    film.append(episode, filmTitle, shortInfo);


    characters.forEach((character) => {
      const characterName = document.createElement("li");
      fetch(character)
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          characterName.textContent = data.name;
          filmCharacters.append(characterName);
        });
    });
    film.append(filmCharacters);
  });
});
